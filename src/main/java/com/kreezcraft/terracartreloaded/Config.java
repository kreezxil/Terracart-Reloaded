package com.kreezcraft.terracartreloaded;

import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.logging.log4j.Level;

import java.io.File;

public class Config {
    final ForgeConfigSpec.BooleanValue use_vanilla_cart;

    Config(final ForgeConfigSpec.Builder builder) {
        builder.push("Cart Config");
        use_vanilla_cart = builder
                .comment("Use vanilla cart friction")
                .define("enable", false);
        builder.pop();
    }
}
